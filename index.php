<!DOCTYPE html>
<html>
<head>
<title>Simple Front & Back End</title>
<script type="text/javascript" src="jquery.min.js"></script>
</head>
<body>

	<h1>Simple Front end</h1>
	<p>Search for user based on user id</p>
	<form id="myform" method="POST">
	<input type="text" id="userid">
	</form>
	<button id="searchuserbutton">Search</button>

<p> Read Data  <p>
<table>	
<thead>
<th>First Name</th>
<th> Password</th>
</thead>
<tbody id="listData">

</tbody>
</table>
<br><br>
<p> Add Data </p>
<table>
<tr>
<td>First Name</td>
<td><input type="text" name="firstname"/></td>
</tr>
<tr>
<td>Password</td>
<td><input type="password" name="password"/></td>
</tr>
<tr>
<td></td>
<td><button onclick="adddata()">Add Data</button></td>
</tr>
</table>
	
	<script>	
	//Server URL 
	var serverUrl ="http://localhost/simplefe/api.php";
	
	//Event Listener when user press searchbutton
	document.getElementById("searchuserbutton").addEventListener("click", processSearchuser);
	//Function called when user press search button
	
	function processSearchuser(){
		//Get value from a form with an id userid
		var userid = document.getElementById("userid").value;
		//Data 
		var dataTopost = "search="+userid;
		var xhr = new XMLHttpRequest();
		xhr.open("POST", serverUrl, true);
		xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
		xhr.onload = function() {
			if (xhr.status == 200) {
				var json = xhr.responseText;
				var response = JSON.parse(json);
				if(response.status == "Successfull"){
					alert("First name: "+response.firstname+" Last name: "+response.lastname);
				}else if(response.status == "Fail"){
					alert(response.status_message);
				}else{
					alert(response.status_message);
				}
			} else if (xhr.status == 404) {
				alert("Fail to connect to our server");
			} else {
				alert("Fail to connect to our server");
			}
		}
		xhr.send(dataTopost);
	}

	
	function adddata(){
	var firstname = $("[name='firstname']").val();
	var password = $("[name='password']").val();
	
	$.ajax({
	type : "POST",
	data : "firstname="+firstname+"&password="+password+"",
	url : 'adddata.php',
	success : function(result){
console.log(result);
	}
	})
	
}

$.ajax({
	type : "GET",
	data : "",
	url : 'api.php',
	success : function(result){
		var objResult=JSON.parse(result);
		$.each(objResult, function(key,val){
		var newList=$("<tr>");
		newList.html("<td>"+val.firstname+"</td><td>"+val.password+"</td>")
		var dataHandler=$("#listData");
		dataHandler.append(newList);
		})
	}
})


	</script>
</body>